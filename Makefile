IMAGE_NAME=4k1l/openjdk
IMAGE_TAG=8-jre-alpine

# These values are changed in each version branch
# This is the only place they need to be changed
# other than the README.md file.

TARGET=$(IMAGE_NAME):$(IMAGE_TAG)
.PHONY: all
all: build  publish

.PHONY: build
build:	Dockerfile bin
		docker build -t $(TARGET) -t $(IMAGE_NAME):latest .
		


.PHONY: publish
publish: all
		echo $(DOCKER_PASS) | docker login -u $(DOCKER_USER) --password-stdin
		docker push $(TARGET)
		docker push $(IMAGE_NAME):latest
